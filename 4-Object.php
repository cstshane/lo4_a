<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>4-Object</title>
    </head>
    <body>
        <h1>4-Object</h1>

<?php

// Open a connection to MySQL
$db = @new mysqli( "kelcstu06", "INS214", "RASSLER", "INS214" );

// If the database connection failed
if ( $db->connect_error )
{
// Then
// 	Perform processing as required to handle the error
    echo "<p>Failed to connect to database: " . $db->connect_error .
            "</p>\n";
    exit();
}

// Else
// 	Go ahead with normal processing on your web page
// 	Close the database connection (call mysqli_close)
else
{
    echo "<p>Successfully connected to the database!</p>\n";
    $db->close();
}

?>

    </body>
</html>
