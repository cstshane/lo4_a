<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>4-Procedural</title>
    </head>
    <body>
        <h1>4-Procedural</h1>

<?php

// Open a connection to MySQL (call mysqli_connect)
$db = @mysqli_connect( "kelcstu06", "INS214", "RASSLER", "INS214" );

// If the database connection failed (mysqli_connect returns false)
if ( !$db )
{
// Then
// 	Perform processing as required to handle the error
    echo "<p>Failed to connect to database: " . mysqli_connect_error() .
            "</p>\n";
    exit();
}

// Else
// 	Go ahead with normal processing on your web page
// 	Close the database connection (call mysqli_close)
else
{
    echo "<p>Successfully connected to the database!</p>\n";
    mysqli_close( $db );
}

?>

    </body>
</html>
