<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Learning Outcome 4</title>
    </head>
    <body>
        <h1>Learning Outcome 4</h1>
        <ol>
            <li><a href="4-Procedural.php">The Procedural Approach</a></li>
            <li><a href="4-Object.php">The Object-oriented Approach</a></li>
            <li><a href="4-OurObject.php">
                    The Object-oriented Approach with our own object</a></li>
        </ol>
    </body>
</html>