<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>4-OurObject</title>
    </head>
    <body>
        <h1>4-OurObject</h1>

<?php
require_once __DIR__ . '/Autoloader.php';
$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_A",
        __DIR__ . '/../../private/CSTClasses_A' );

use CSTClasses_A\DbObject;

$db = new DbObject();
echo "<p>Successfully connected to the database!</p>\n";

?>

    </body>
</html>
